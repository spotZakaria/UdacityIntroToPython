#-------------------------------------------------------------------------------
# Name:        UPDrawArt
# Purpose:
#
# Author:      sassiouizakaria
#
# Created:     05/10/2016
# Copyright:   (c) 123 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import turtle

def drawArt():

    window = turtle.Screen()
    window.bgcolor("red")

    spot = turtle.Turtle()
    spot.shape("circle")
    spot.color("black")
    spot.speed(1)
    spot.setheading(300)

    for i  in range(0,36):
        t = True
        for x in range(0,4):
            spot.forward(50)
            if(t):
                spot.right(45)
                t = False
            else:
                spot.right(135)
                t = True
        spot.right(10)
    spot.forward(350)

    window.exitonclick()

drawArt()

